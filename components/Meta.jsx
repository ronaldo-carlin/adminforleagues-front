import Head from "next/head";

const Meta = ({ title, keyword, desc }) => {
  return (
    <div>
      <Head>
        <title>{title} || Admin For Leangues🏆</title>
        <link rel="icon" href="/favicon.png" />
        <meta name="description" content={desc} />
        <meta name="keyword" content={keyword} />
      </Head>
    </div>
  );
};

Meta.defaultProps = {
  title: "AR | Admin For Leangues🏆",
  keyword:
    "Administrador de ligas de fútbol, Gestión de torneos, Programación de partidos, Estadísticas de fútbol, Seguimiento de equipos, Resultados en tiempo real, Calendario de partidos, Comunicación deportiva, Inscripción de equipos, Personalización de reglas, Interfaz amigable, Herramientas de comunicación, Desempeño de jugadores, Goleadores y asistencias, Organizador de campeonatos, Planificación automática de partidos, Gestión de jugadores, Notificaciones en tiempo real, Emoción del juego, Experiencia única de liga de fútbol",
  desc: "¡Bienvenido a El Administrador de Ligas, la solución integral para gestionar de manera eficiente y emocionante tus ligas de fútbol! Diseñada para simplificar el proceso de organización y seguimiento de torneos, nuestra plataforma ofrece herramientas avanzadas que permiten a los administradores, entrenadores y jugadores disfrutar al máximo de la experiencia futbolística.",
};

export default Meta;
